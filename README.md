# GCC toolchain build script

This is a script to build GCC toolchains targeting arm, arm64, and x86 devices
(primarily Android devices).


## Using the script

To build a GCC toolchain, you will need the following:

+ A Linux distribution (the script has been tested on Debian 10 and Arch Linux)
+ A decent processor and amount of RAM (i5 and 8 GB of RAM or more is preferred)
+ Core developer packages
    + For Arch: ```sudo pacman -S base-devel git```
    + For Debian: ```sudo apt-get install flex bison libncurses-dev texinfo gcc gperf patch libtool g++ libncurses5-dev gawk expat libexpat1-dev python-all-dev binutils-dev lib32gcc1 bc libisl-dev libcap-dev libgmp-dev build-essential gcc-multilib g++-multilib pkg-config libmpc-dev libmpfr-dev autopoint gettext liblzma-dev libssl-dev zlib1g-dev zstd libzstd-dev```
+ Some systems might require installing ```curl```, ```wget``` and ```rsync```
  manually, especially on a clean chroot.

Once you have set up your environment, run the following:

```bash
git clone https://github.com/krasCGQ/build-tools-gcc
cd build-tools-gcc
./build -h
```

The printout will show you how to run the script.

Example commands:

```bash
# Build a GNU 12.x toolchain for x86_64
./build -a x86_64 -s gnu -v 12

# Build an Arm 11.x toolchain for arm64, while providing full output
./build -a arm64 -s linaro -v 11 -V

# Build a Linaro 7.x toolchain for arm, while providing full output
./build -a arm -s linaro -v 7 -V
```


## After compilation

Once it is done building, you will have a folder with the compiled toolchain
which you can then move to directory of your choice.

Point your cross compiler to the proper file and compile! This is an easy
shortcut for kernels (when run in the directory you moved the toolchain in):

```bash
# The following sample is for arm64
PATH=$(pwd)/bin:$PATH
CROSS_COMPILE=aarch64-linux-gnu-
export PATH CROSS_COMPILE
```


## Pull requests/issues

If you have any issues with this script, feel free to open an issue!

Pull requests are more than welcome as well. However, there is a particular coding style that should be followed:

+ All variables are uppercased and use curly braces: ```${VARIABLE}``` instead of ```$variable```
+ Four spaces for indents
+ Double brackets and single equal sign for string comparisons in if blocks: ```if [[ ${VARIABLE} = "yes" ]]; then```

Additionally, please be sure to run your change through shellcheck.net (either copy and paste the script there or download the binary and run `shellcheck build`).


## Credits/thanks

+ [USBHost](https://github.com/USBhost): For the initial script
+ [nathanchance](https://github.com/nathanchance): For great modifications!
+ [frap129](https://github.com/frap129): For some modifications to update the script/components
+ [msfjarvis](https://github.com/msfjarvis): For testing the arm option
